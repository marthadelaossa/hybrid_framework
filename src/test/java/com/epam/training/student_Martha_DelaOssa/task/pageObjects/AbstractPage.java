package com.epam.training.student_Martha_DelaOssa.task.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public abstract class AbstractPage
{
	protected WebDriverWait wait;
	protected WebDriver driver;

	protected abstract AbstractPage openPage();

	protected AbstractPage(WebDriver driver, WebDriverWait wait)
	{
		this.driver = driver;
		this.wait = new WebDriverWait(driver, Duration.ofMillis(10000));
	}
}
