package com.epam.training.student_Martha_DelaOssa.task.driver;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class DriverOptions {


    private static WebDriver driver;

    private static WebDriverWait wait;


    public  static String ensureBrowserPropertySet() {
        String browser = System.getProperty("browser");
        if (browser == null) {
            System.setProperty("browser", "chrome");

        }
        return  browser;
    }

    public static WebDriver getDriver(){
        System.setProperty("browser", "chrome");
        if (null == driver){
            switch (ensureBrowserPropertySet()){
                case "firefox": {
                    WebDriverManager.firefoxdriver().setup();
                    driver = new FirefoxDriver();
                }
                default: {
                    WebDriverManager.chromedriver().setup();
                    driver = new ChromeDriver(options());
                }
            }
            driver.manage().window().maximize();
        }
        return driver;
    }


    public static ChromeOptions options(){
        ChromeOptions options=new ChromeOptions();
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-infobars");
        options.addArguments("--incognito");
        return (options);
    }

    public static WebDriverWait  getWait(){
        wait = new WebDriverWait(driver, Duration.ofMillis(4000));
        return wait;
    }

    public static void closeDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
