package com.epam.training.student_Martha_DelaOssa.task.test;



import com.epam.training.student_Martha_DelaOssa.task.pageObjects.CalculatorPage;
import com.epam.training.student_Martha_DelaOssa.task.pageObjects.HomePage;
import com.epam.training.student_Martha_DelaOssa.task.pageObjects.ResultPage;
import com.epam.training.student_Martha_DelaOssa.task.pageObjects.SummaryPage;
import org.testng.annotations.Test;


public class GoogleCloudTest extends CommonConditions {



    @Test(description = "Search for Pricing Calculator at https://cloud.google.com/. ", priority=1)
    public void search() {
        HomePage homepage = new HomePage()
                .openPage()
                .search();
    }

    @Test(description = "Go to Pricing Calculator", priority=2)
    public void clickResult() {
        ResultPage resultpage = new ResultPage()
                .goToSearchResults();
    }

    @Test(description = "Pricing Calculator", priority=3)
    public void calculator() {
        CalculatorPage calculatorPage = new  CalculatorPage()
                .addEstimated()
                .computedEngine();

   }

  @Test (description = "Data Validation", priority=4)
    public void summary()  {
      SummaryPage summaryPage = new  SummaryPage()
              .validateData();
  }
}
