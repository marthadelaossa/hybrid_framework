package com.epam.training.student_Martha_DelaOssa.task.pageObjects;

import com.epam.training.student_Martha_DelaOssa.task.utils.WebMethods;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import static com.epam.training.student_Martha_DelaOssa.task.driver.DriverOptions.getDriver;
import static com.epam.training.student_Martha_DelaOssa.task.driver.DriverOptions.getWait;


public class HomePage extends AbstractPage {

    private static final String HOMEPAGE_URL=" https://cloud.google.com/";
    private static final String TEXT_TO_SEARCH = "Google Cloud Platform Pricing Calculator";

    private final By INPUT_SEARCH_BOX = By.xpath("//input[@class ='mb2a7b']");

    public HomePage() {
        super(getDriver(), getWait());
        PageFactory.initElements(this.driver, this);
    }

    @Override
    public  HomePage openPage() {
        driver.navigate().to(HOMEPAGE_URL);
        WebMethods.waitForPageLoad();
        return this;
    }

    public  HomePage search(){
        WebMethods.sendKey(TEXT_TO_SEARCH,INPUT_SEARCH_BOX);
        WebMethods.enter(INPUT_SEARCH_BOX);
        return this;
    }
}
