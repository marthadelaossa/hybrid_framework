package com.epam.training.student_Martha_DelaOssa.task.pageObjects;

import com.epam.training.student_Martha_DelaOssa.task.utils.WebMethods;
import org.openqa.selenium.By;


public class CalculatorPage {

    private static final String NUMBER_INSTANCES = "4";

    private final By BTN_ADD_ESTIMATE = By.xpath("//button[child::*[contains(text(),'Add to estimate')]]");
    private final By BTN_COMPUTE_ENGINE = By.xpath("//*[@tabindex='0'][child::*[text()='Compute Engine']]");

    private final By BTN_ADD_TO_ESTIMATE = By.xpath("//*[@class='jirROd'][descendant::*[text()='Add to estimate']]");
    private final By INPUT_NUMBER_INSTANCES = By.xpath("//input[@class='qdOxv-fmcmS-wGMbrd'][ancestor::*[@class='QiFlid']]");

    private final By LST_OPERATING_SYSTEM = By.xpath("(//*[@class='VfPpkd-aPP78e'])[4]");
    private final By UL_OPERATING_SYSTEM = By.xpath("//li[@tabindex='0']");

    private final By BTN_REGULAR = By.xpath("//*[child::*[text()='Regular']]");

    private final By LST_MACHINE_FAMILY = By.xpath("(//*[@class='VfPpkd-aPP78e'])[5]");
    private final By UL_MACHINE_FAMILY = By.xpath("//li[@data-value='general-purpose']");

    private final By LST_SERIES = By.xpath("(//*[@class='VfPpkd-aPP78e'])[6]");
    private final By UL_SERIES = By.xpath("//li[@data-value='n1']");

    private final By LST_MACHINE_TYPE = By.xpath("(//*[@class='VfPpkd-aPP78e'])[7]");
    private final By UL_MACHINE_TYPE = By.xpath("//li[@data-value='n1-standard-8']");

    private final By BTN_ADD_GPUS = By.xpath("//span[@class='eBlXUe-hywKDc'][ancestor::*[@aria-label='Add GPUs']]");

    private final By LST_GPU_MODEL = By.xpath("(//*[@class='VfPpkd-aPP78e'])[8]");
    private final By UL_GPU_MODEL = By.xpath("//li[@data-value='nvidia-tesla-v100']");

    private final By LST_NUMBERS_GPU= By.xpath("(//*[@class='VfPpkd-aPP78e'])[9]");
    private final By UL_NUMBERS_GPU = By.xpath("(//li[@data-value='1'])[1]");

    private final By LST_LOCAL_SSD= By.xpath("(//*[@class='VfPpkd-aPP78e'])[10]");
   private final By UL_LOCAL_SSD = By.xpath("(//li[@data-value='2'])[2]");

   private final By LST_REGION= By.xpath("(//*[@class='VfPpkd-aPP78e'])[11]");
   private final By UL_REGION = By.xpath("//li[@data-value='us-central1']");

   private final By BTN_COMMITED_USE = By.xpath("//*[child::*[text()='1 year']]");

   private final By COMODIN= By.xpath("(//*[@class='VfPpkd-aPP78e'])[13]");

   private final By BTN_CLOSE_ADD_ESTIMATE= By.xpath("//button[@aria-label='Cerrar cuadro de diálogo']");

   private final By BTN_SHARE= By.xpath("//button[@aria-label='Open Share Estimate dialog']");

   private final By BTN_OPEN_SUMARY= By.xpath("//a[@track-name='open estimate summary']");

    public  CalculatorPage addEstimated(){
        WebMethods.waitForPageLoad();
        WebMethods.click(BTN_ADD_ESTIMATE);
        WebMethods.click(BTN_COMPUTE_ENGINE);
        return this;
    }

    public  CalculatorPage computedEngine(){
        WebMethods.waitForPageLoad();
        WebMethods.findElement(BTN_ADD_TO_ESTIMATE);
        WebMethods.clearInput(INPUT_NUMBER_INSTANCES);
        WebMethods.sendKey(NUMBER_INSTANCES,INPUT_NUMBER_INSTANCES);

        WebMethods.click(LST_OPERATING_SYSTEM);
        WebMethods.click(UL_OPERATING_SYSTEM);

        WebMethods.click(BTN_REGULAR);

        WebMethods.click(LST_MACHINE_FAMILY);
        WebMethods.click(UL_MACHINE_FAMILY);

        WebMethods.click(LST_SERIES);
        WebMethods.click(UL_SERIES);

        WebMethods.click(LST_MACHINE_TYPE);
        WebMethods.click(UL_MACHINE_TYPE);

        WebMethods.click(BTN_ADD_GPUS);

        WebMethods.findElement(COMODIN);
        WebMethods.click(LST_GPU_MODEL);
        WebMethods.click(UL_GPU_MODEL);

        WebMethods.click(LST_NUMBERS_GPU);
        WebMethods.click(UL_NUMBERS_GPU);

        WebMethods.click(LST_LOCAL_SSD);
        WebMethods.click(UL_LOCAL_SSD);

        WebMethods.click(LST_REGION);
        WebMethods.click(UL_REGION);

        WebMethods.click(BTN_COMMITED_USE);

        WebMethods.scrollTop();

        WebMethods.click(BTN_ADD_ESTIMATE);

        WebMethods.scrollToButton(BTN_SHARE);

        WebMethods.findElement(BTN_OPEN_SUMARY);
        WebMethods.click(BTN_OPEN_SUMARY);
        return this;
    }
}
