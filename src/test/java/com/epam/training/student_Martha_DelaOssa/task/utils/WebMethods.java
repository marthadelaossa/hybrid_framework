package com.epam.training.student_Martha_DelaOssa.task.utils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;


import static com.epam.training.student_Martha_DelaOssa.task.driver.DriverOptions.*;

public class WebMethods {

     public static void waitForPageLoad() {
          getWait().until(jQueryAJAXsCompleted());
     }

     public static ExpectedCondition<Boolean> jQueryAJAXsCompleted() {
          return driver -> {
               JavascriptExecutor jsExecutor = (JavascriptExecutor) getDriver();
               return (Boolean) jsExecutor.executeScript("return document.readyState").equals("complete");
          };}



     public static WebElement findElement(By locator) {
          getWait().until(ExpectedConditions.presenceOfElementLocated(locator));
          return getDriver().findElement(locator);
     }



     public static void sendKey(CharSequence pressKeys, By locator) {
          getWait().until(ExpectedConditions.presenceOfElementLocated(locator));
          findElement(locator).sendKeys(pressKeys);
     }

     public static void clearInput(By locator) {
          getWait().until(ExpectedConditions.presenceOfElementLocated(locator));
          findElement(locator).clear();
     }

     public static void  click (By locator) {
          getWait().until(ExpectedConditions.elementToBeClickable(locator));
          findElement(locator).click();
     }

     public static void enter(By locator) {
          getWait().until(ExpectedConditions.elementToBeClickable(locator));
          findElement(locator).submit();
     }
     public static void  scrollTop () {
          JavascriptExecutor js = (JavascriptExecutor) getDriver();
           js.executeScript("window.scrollTo(0, -document.body.scrollHeight)");

     }

     public static void  scrollToButton (By locator) {
          JavascriptExecutor js = (JavascriptExecutor) getDriver();
          js.executeScript("arguments[0].scrollIntoView(true);",  getWait().until(ExpectedConditions.elementToBeClickable(locator)));
          js.executeScript("arguments[0].click();",   getWait().until(ExpectedConditions.elementToBeClickable(locator)));

     }

     public static  boolean elementVisible(By locator) {
          JavascriptExecutor js = (JavascriptExecutor) getDriver();
          js.executeScript("arguments[0].scrollIntoView(true);",  getWait().until(ExpectedConditions.presenceOfElementLocated(locator)));
          return getDriver().findElement(locator).isDisplayed();

     }

}