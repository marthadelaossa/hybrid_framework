package com.epam.training.student_Martha_DelaOssa.task.test;

import com.epam.training.student_Martha_DelaOssa.task.driver.DriverOptions;
import com.epam.training.student_Martha_DelaOssa.task.utils.TestListener;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

@Listeners({TestListener.class})
public class CommonConditions {

    protected static WebDriver driver;
    protected static WebDriverWait wait;

    @BeforeSuite()
    public  void setUp()
    {
        driver = DriverOptions.getDriver();
        wait=DriverOptions.getWait();
    }

    @AfterSuite()
    public void stopBrowser()
    {
        DriverOptions.closeDriver();
    }
}
