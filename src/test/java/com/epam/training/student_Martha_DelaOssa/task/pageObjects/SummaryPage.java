package com.epam.training.student_Martha_DelaOssa.task.pageObjects;

import com.epam.training.student_Martha_DelaOssa.task.utils.WebMethods;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;

public class SummaryPage {

    private final By LBL_ESTIMATE_COST = By.xpath("//span[contains(text(),'5,413.26')]");

    public  SummaryPage validateData(){
        WebMethods.waitForPageLoad();
        Assertions.assertTrue( WebMethods.elementVisible(LBL_ESTIMATE_COST),"Product Cost in not correct");
        return this;
    }
}
