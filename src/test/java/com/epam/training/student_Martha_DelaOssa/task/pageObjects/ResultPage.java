package com.epam.training.student_Martha_DelaOssa.task.pageObjects;

import com.epam.training.student_Martha_DelaOssa.task.utils.WebMethods;
import org.openqa.selenium.By;

public class ResultPage {

    private final By LINK_SEARCH_RESULT = By.xpath("//a[@class='gs-title'][child::*[text()='Google Cloud Pricing Calculator']]");

    public  ResultPage goToSearchResults(){
        WebMethods.waitForPageLoad();
        WebMethods.click(LINK_SEARCH_RESULT);
        return this;
    }
}
